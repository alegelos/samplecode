//
//  String+Addition.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/7/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import Foundation

extension String{
    func removeHTTPs() -> String {
        var purgedURL = replacingOccurrences(of: "https", with: "")
        purgedURL = purgedURL.replacingOccurrences(of: "http",  with: "")
        
        return purgedURL
    }
}
