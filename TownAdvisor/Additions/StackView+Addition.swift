//
//  StackView+Addition.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/6/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit

extension UIStackView{
    func removeAllItems() {
        for item in arrangedSubviews {
            removeArrangedSubview(item)
            item.removeFromSuperview()
        }
    }
    
    func addItems(items: Array<UIView>?) {
        guard let items = items else{ return }
        for item in items {
            addArrangedSubview(item)
        }
    }
}
