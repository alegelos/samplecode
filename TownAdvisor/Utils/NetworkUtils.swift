//
//  NetworkConfiguration.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import Foundation
import PromiseKit

struct NetworkUtils {
    enum EndPoint:String{
        case baseURL, getProfiles
    }
    
    static func getURLRequest(serviceType service:EndPoint) -> URL? {
        guard let servicesDic = PListUtils.getDic(fromPList: .Services),
              let baseURL = servicesDic[EndPoint.baseURL.rawValue] as? String else {
                return nil
        }
        
        switch service {
        case .baseURL:
            return URL(string: baseURL)
        case .getProfiles:
            guard let getProfiles = servicesDic[service.rawValue] as? String else{ return nil}
            return URL(string: baseURL+getProfiles)
        }
    }
    
    static func getURLRequest(serviceType service:EndPoint) -> Promise<URL>{
        return Promise{ seal in
            guard let url:URL = getURLRequest(serviceType: service) else{
                seal.reject(NetworkError.failTogetUrlRequest)
                return
            }
            
            seal.fulfill(url)
        }
    }
    
    
}
