//
//  Errors.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import Foundation


enum NetworkError: Error {
    case failTogetUrlRequest, failToParseResponse, failToGetImageFromData
}
