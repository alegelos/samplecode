//
//  StackViewUtils.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/7/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit

class StackViewUtils {
    class func getItemsLabelsArray(withItems items: Array<String>?, fontSize:Int=9, textColor: UIColor=UIColor.white) -> Array<UILabel>?{
        guard let items = items else {return nil}
        
        var itemssLabelArray:Array<UILabel> = []
        
        for item in items {
            itemssLabelArray.append(getItemLabel(withTitle: item, fontSize: fontSize, textColor: textColor))
        }
        return itemssLabelArray
    }
    
    static private func getItemLabel(withTitle title: String, fontSize: Int, textColor:UIColor) -> UILabel{
        let newLabel  = UILabel()
        
        newLabel.text = title
        newLabel.font = newLabel.font.withSize(CGFloat(fontSize))
        newLabel.textColor = textColor
        
        return newLabel
    }
    
}
