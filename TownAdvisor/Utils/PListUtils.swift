//
//  PListUtils.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import Foundation

enum PListName:String {
    case Services
}

struct PListUtils {
    static func getDic(fromPList pList: PListName) -> Dictionary<String, Any>?{
        guard let dicPath = Bundle.main.path(forResource: pList.rawValue, ofType: "plist") else{
            return nil
        }
        
        return NSDictionary(contentsOfFile: dicPath) as? Dictionary<String, Any>
    }
}
