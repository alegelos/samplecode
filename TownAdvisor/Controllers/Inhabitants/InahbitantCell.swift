//
//  InahbitantCell.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/6/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit

class InahbitantCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var professionsStackView: UIStackView!
    @IBOutlet weak var professionsDarkView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var thumbnail:String?
    var professions:Array<String>? {
        didSet{
            setProfessions(professions: professions)
        }
    }
    
    func loadCell(withInhavitant inhabitant: Inhabitant) {
        name.text   = inhabitant.name
        professions = inhabitant.professions
        thumbnail   = inhabitant.thumbnail
        
        if let age = inhabitant.age{
            self.age.text    = String(describing: age)
        }
        
        image.image = nil
        if let thumbnail = inhabitant.thumbnail{
            loadInhabitantImage(thumbnail: URL(string: thumbnail))
        }
    }
}


//MARK: - Private Methods
extension InahbitantCell{
    private func setProfessions(professions: Array<String>?) {
        professionsStackView.removeAllItems()
        professionsStackView.addItems(items: StackViewUtils.getItemsLabelsArray(withItems: professions))
    }
    
    private func loadInhabitantImage(thumbnail: URL?){
        guard let imageURL = thumbnail else{
            return
        }
        
        loadingIndicator.startAnimating()
        NetworkingManager.shared.getImage(fromURL: imageURL)
        .done{ [weak self] args in
            guard args.requestURL == self?.thumbnail?.removeHTTPs() else{
                return
            }
            self?.image.image = args.image
        }.ensure { [weak self] in
            self?.loadingIndicator.stopAnimating()
        }.catch{ [weak self] error in
            self?.image.image = #imageLiteral(resourceName: "NoProfileImage")
        }
    }
}
