//
//  ViewController.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit
import PromiseKit


class InhabitantsVC: UIViewController {

    @IBOutlet weak var inhavitantsCV: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBarSpaceToCVContraint: NSLayoutConstraint!
    
    private var selectedInhabitant: Inhabitant?
    
    private let kTownKey = "Brastlewark"
    private let kSegueToInhabitantDetails = "goToInhabitantDetails"
    private let collectionSource = InhabitantsCollectionSource()
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        inhavitantsCV.dataSource  = collectionSource
        inhavitantsCV.delegate    = collectionSource
        collectionSource.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        definesPresentationContext = true
        
        loadInhabitants()//TODO: Call if netowrk status change
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let inhabitantDetails as InhabitantDetails:
            inhabitantDetails.inhabitant = selectedInhabitant
        default:
            break
        }
    }
    
    @IBAction func didTapInhabitantsCollection(_ sender: Any) {
        searchController.searchBar.resignFirstResponder()
    }
}


//MARK: - InhavitantsCollectionSource Delegates
extension InhabitantsVC: InhabitantsCollectionSourceDelegate{
    func didSelect(item: Inhabitant) {
        selectedInhabitant = item
        performSegue(withIdentifier: kSegueToInhabitantDetails, sender: self)
    }
}

extension InhabitantsVC: UISearchResultsUpdating{    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else{
            return
        }
        collectionSource.filterInhabitants(withText: searchText)
        inhavitantsCV.reloadData()
    }
}

//MARK: - Private Methods
extension InhabitantsVC{
    private func loadInhabitants(){
        loadingIndicator.startAnimating()
        _ = NetworkingManager.shared.getProfiles()
        .done{ [weak self] townsData in
            guard let kTownKey = self?.kTownKey, let inhabitants = townsData[kTownKey] else{
                return
            }
        
            self?.collectionSource.inhabitants = inhabitants
            self?.title = self?.kTownKey
            self?.inhavitantsCV.reloadData()
        }.ensure { [weak self] in
            self?.loadingIndicator.stopAnimating()
        }
    }
}

