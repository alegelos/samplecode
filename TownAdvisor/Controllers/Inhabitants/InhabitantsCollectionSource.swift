//
//  InhavitantsCollectionSource.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit
import PromiseKit

protocol  InhabitantsCollectionSourceDelegate: class{
    func didSelect(item: Inhabitant)
}

class InhabitantsCollectionSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let kInhavitantCellIdentifier = "InhabitantCell"
    var inhabitants:[Inhabitant] = [] {
        didSet{
            resetFilterInhabitants()
        }
    }
    private var filterinhabitantsIndex:[Int] = []
    
    weak var delegate: InhabitantsCollectionSourceDelegate?
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterinhabitantsIndex.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kInhavitantCellIdentifier, for: indexPath) as? InahbitantCell else{
            return UICollectionViewCell()
        }
        
        cell.loadCell(withInhavitant: inhabitants[filterinhabitantsIndex[indexPath.item]])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(item: inhabitants[filterinhabitantsIndex[indexPath.item]])
    }
    
    func filterInhabitants(withText searchText:String){
        if searchText == ""{
            if filterinhabitantsIndex.count != inhabitants.count{
                resetFilterInhabitants()
            }
        }else{
            filterinhabitantsIndex = getFilterInhabitants(withText: searchText)
        }
    }
}

//MARK: - PrivateMethdods
extension InhabitantsCollectionSource{
    private func getFilterInhabitants(withText searchText:String) -> Array<Int>{
        filterinhabitantsIndex.removeAll()
        for enumeratedInhabitan in inhabitants.enumerated() {
            if let name = enumeratedInhabitan.element.name, name.lowercased().contains(searchText.lowercased()){
                filterinhabitantsIndex.append(enumeratedInhabitan.offset)
                continue
            }
            
            if let professions = enumeratedInhabitan.element.professions,
                filterProfessions(withSearchText: searchText, inProfessions: professions){
                    filterinhabitantsIndex.append(enumeratedInhabitan.offset)
                    continue
            }
            
            if let age = enumeratedInhabitan.element.age, String(age).contains(searchText){
                filterinhabitantsIndex.append(enumeratedInhabitan.offset)
                continue
            }
            
            if let hairColor = enumeratedInhabitan.element.hair_color, hairColor.contains(searchText){
                filterinhabitantsIndex.append(enumeratedInhabitan.offset)
                continue
            }
        }
        return filterinhabitantsIndex
    }
    
    private func resetFilterInhabitants(){
        filterinhabitantsIndex = inhabitants.enumerated().map{ (index, elem) in index }
    }
    
    private func filterProfessions(withSearchText searchText:String, inProfessions professions: Array<String>)->Bool{
        let professionsLowerCased = professions.map{$0.lowercased()}
        for professionLowerCased in professionsLowerCased{
            if professionLowerCased.contains(searchText.lowercased()){
                return true
            }
        }
        return false
    }
}
