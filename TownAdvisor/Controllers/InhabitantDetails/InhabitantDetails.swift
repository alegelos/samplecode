//
//  InhabitantDetails.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/7/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit

class InhabitantDetails: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var hairColor: UILabel!
    @IBOutlet weak var professionsStackView: UIStackView!
    @IBOutlet weak var friendsStackView: UIStackView!
    
    var inhabitant:Inhabitant?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        name.text = inhabitant?.name
        
        if let age = inhabitant?.age{
            self.age.text = String(describing: age)
        }
        
        if let weight = inhabitant?.weight{
            self.weight.text = "Weight: " + String(describing: weight)
        }
        
        if let height = inhabitant?.height{
            self.height.text = "Height: " + String(describing: height)
        }
        
        if let hairColor = inhabitant?.hair_color{
            self.hairColor.text = "Hair color: " + String(describing: hairColor)
        }
        
        image.image = nil
        if let thumbnail = inhabitant?.thumbnail{
            loadInhabitantImage(thumbnail: URL(string: thumbnail))
        }
        
        loadWithItems(stackView: professionsStackView, withItems: inhabitant?.professions)
        loadWithItems(stackView: friendsStackView, withItems: inhabitant?.friends)   
    }
}

//MARK: - Private Methods
extension InhabitantDetails{
    private func loadInhabitantImage(thumbnail: URL?){
        guard let imageURL = thumbnail else{
            return
        }
        _ = NetworkingManager.shared.getImage(fromURL: imageURL)
        .done{ [weak self] imageData -> () in
            self?.image.image = imageData.1
        }
    }
    
    private func loadWithItems(stackView: UIStackView, withItems items: Array<String>?) {
        stackView.removeAllItems()
        stackView.addItems(items: StackViewUtils.getItemsLabelsArray(withItems: items, fontSize: 12, textColor: .black))
    }
}
