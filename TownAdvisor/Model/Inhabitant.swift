//
//  Inhabitant.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import Foundation

struct Inhabitant {
    let id:Int?
    let name: String?
    let thumbnail:String?
    let age: Int?
    let weight:Float?
    let height: Float?
    let hair_color: String?
    let professions: Array<String>?
    let friends: Array<String>?
    
    init(withDic dic: Dictionary<String,Any>) {
        id          = dic["id"]          as? Int
        name        = dic["name"]        as? String
        thumbnail   = dic["thumbnail"]   as? String
        age         = dic["age"]         as? Int
        weight      = dic["weight"]      as? Float
        height      = dic["height"]      as? Float
        hair_color  = dic["hair_color"]  as? String
        professions = dic["professions"] as? Array<String>
        friends     = dic["friends"]     as? Array<String>
    }
}
