//
//  etworkingManager.swift
//  TownAdvisor
//
//  Created by Alejandro Gelos on 3/5/18.
//  Copyright © 2018 D-Software. All rights reserved.
//

import UIKit
import PromiseKit
import Alamofire

class NetworkingManager {
    
    open static let shared = NetworkingManager()
    private init(){}
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    private var activeNetworkProcess = 0 {
        didSet{
            UIApplication.shared.isNetworkActivityIndicatorVisible = activeNetworkProcess > 0
        }
    }
    
    @discardableResult func getProfiles() -> Promise<Dictionary<String,Array<Inhabitant>>> {
        return firstly{
            NetworkUtils.getURLRequest(serviceType: .getProfiles)
        }.then{ [weak self] url -> Promise<(json: Any, response: PMKAlamofireDataResponse)> in
            self?.updateNetworkActivityCounter(numberOfProcess: 1)
            return Alamofire.request(url, method: .get).responseJSON()
        }.then{ args -> Promise<Dictionary<String,Array<Inhabitant>>> in
            self.getProfilesDicFromResponse(json: args.json)
        }.ensure { [weak self] in
           self?.updateNetworkActivityCounter(numberOfProcess: -1)
        }
    }
    
    @discardableResult func getImage(fromURL requestURL:URL) -> Promise<(requestURL: String, image: UIImage)> {
            if let cachedImage = self.imageCache.object(forKey: requestURL.absoluteString.removeHTTPs() as NSString) {
                return Promise { seal in
                    seal.fulfill((requestURL.absoluteString.removeHTTPs(), cachedImage))
                }
            }
            
            updateNetworkActivityCounter(numberOfProcess: 1)
            
            return firstly{
                Alamofire.request(requestURL, method: .get).responseData()
            .then{ args -> Promise<(requestURL: String, image: UIImage)> in
                self.getImageFromResponse(data: args.data, rps: args.response)
            }.ensure { [weak self] in 
                self?.updateNetworkActivityCounter(numberOfProcess: -1)
            }
        }
    }
}

//MARK: - Private Methods
extension NetworkingManager{
    private func updateNetworkActivityCounter(numberOfProcess amount:Int){
        activeNetworkProcess = activeNetworkProcess + amount
    }
    
    private func getProfilesDicFromResponse(json:Any)->Promise<Dictionary<String,Array<Inhabitant>>>{
        return Promise{ seal in
            guard let dic = json as? [String:Any] else{
                seal.reject(NetworkError.failToParseResponse)
                return
            }
            
            var townsDic = Dictionary<String, Array<Inhabitant>>()
            
            for town in dic{
                guard let responseDic = town.value as? Array<Dictionary<String,Any>> else{
                    continue
                }
                
                var inhabitants:Array<Inhabitant> = []
                for inhabitantDic in responseDic{
                    inhabitants.append(Inhabitant(withDic: inhabitantDic))
                }
                townsDic[town.key] = inhabitants
            }
            seal.fulfill(townsDic)
        }
    }
    
    private func getImageFromResponse(data: Data, rps: PMKAlamofireDataResponse) -> Promise<(requestURL: String, image: UIImage)>{
        return Promise{ seal in
            guard let image = UIImage(data: data),
                let responseURL = rps.response?.url?.absoluteString.removeHTTPs() else{
                    seal.reject(NetworkError.failToGetImageFromData)
                    return
            }
            self.imageCache.setObject(image, forKey: responseURL as NSString)
            seal.fulfill((responseURL, image))
        }
    }
}
